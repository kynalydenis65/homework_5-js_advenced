// index.js
import { Card } from './card.js';

// Функція для завантаження користувачів
async function fetchUsers() {
	try {
		const response = await fetch('https://ajax.test-danit.com/api/json/users');
		const users = await response.json();
		return users;
	} catch (error) {
		console.error('Error fetching users:', error);
		throw error;
	}
}

// Функція для завантаження постів та відображення їх на сторінці
async function fetchAndDisplayPosts() {
	try {
		const users = await fetchUsers();
		const response = await fetch('https://ajax.test-danit.com/api/json/posts');
		const posts = await response.json();

		const postsContainer = document.getElementById('posts-container');
		posts.forEach(post => {
			const user = users.find(user => user.id === post.userId);
			post.user = user;
			const card = new Card(post);
			postsContainer.appendChild(card.element);
		});
	} catch (error) {
		console.error('Error fetching posts:', error);
	}
}

// Виклик функції для завантаження постів та їх відображення на сторінці
fetchAndDisplayPosts();
