// card.js
export class Card {
	constructor(post) {
		this.post = post;
		this.element = this.createCardElement();
	}

	createCardElement() {
		const card = document.createElement('div');
		card.classList.add('card');

		const title = document.createElement('h2');
		title.textContent = this.post.title;

		const text = document.createElement('p');
		text.textContent = this.post.text;

		const userInfo = document.createElement('div');
		userInfo.textContent = `${this.post.user.firstName} ${this.post.user.lastName} - ${this.post.user.email}`;

		const deleteButton = document.createElement('span');
		deleteButton.classList.add('delete-button');
		deleteButton.textContent = '❌';
		deleteButton.onclick = () => this.deleteCard();

		card.appendChild(title);
		card.appendChild(text);
		card.appendChild(userInfo);
		card.appendChild(deleteButton);

		return card;
	}

	deleteCard() {
		fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
			method: 'DELETE'
		})
			.then(response => {
				if (response.ok) {
					this.element.remove();
				} else {
					console.error('Failed to delete post');
				}
			})
			.catch(error => {
				console.error('Error deleting post:', error);
			});
	}
}
